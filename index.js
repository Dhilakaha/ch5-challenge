/*
File js untuk web service
*/

const express = require('express');
var cors = require('cors')
const app = express();
let data = require('./data.json');

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(express.static('public'));
app.use(cors());

app.get('/api/v1/images', (req, res) => {
    res.status(200).json(data);
});

app.get('/game', (req, res) => {
    res.sendFile('game.html', { root: __dirname });
})

app.get('/home', (req, res) => {
    res.sendFile('index.html', { root: __dirname });
})

app.get('/', (req, res) => {
    iniError
})

app.listen(3000,() => {
    console.log('localhost:3000/home');
});

app.use(function(err, req, res, next) {
    res.status(404).json({
        status: 'fail',
        errors: 'Are you lost?'
    })
})

//https://codeforgeek.com/render-html-file-expressjs/